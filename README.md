# Web Semantic

- Lucas Tournière 21500503
- Clément Vétillard 21504598

## Prérequis

Optionnellement, vous pouvez utiliser un environnement virtuel pour éviter de polluer l'installation globale de Python :

```bash
python -m venv .env
source .env/bin/activate
```

Pour installer les dépendances :
`pip install -r requirements.txt`

Pour vérifier si SpaCy possède le bon model :

```bash
python -m spacy validate
```

Vous devriez voir apparaitre  :

- [x] fr_core_news_lg

Si ce n'est pas le cas, exécuter cette commande :

`python -m spacy download fr_core_news_lg`

Le modèle `lg` pèse environs 550Mo, vous pouvez utiliser le modèle `md` ou `sm` qui sont plus petit.

## Utilisation

Pour utiliser l'application, utilisez simplement le script *src/main.py*. 

Exemple d'utilisation :
```bash
$ python3 src/main.py -h
usage: main.py [-h] [research [research ...]]

 Search entry in "Le Grand Débat"

  This script will initialize data if they doesn't exist.
  

positional arguments:
  research    Keyword (s) to search

optional arguments:
  -h, --help  show this help message and exit
```

## Problèmes rencontrés

La gestion des accents posent problème lors de la recherche. Whoosh donne l'impression de stocker les chaînes de caractère sous forme de bytes en Python, ce qui modifie l'encodage. L'utf-8 n'étant pas représenter *directement*, on se retrouve avec des caractères du type `\xc5`.

Lorsque l'on recherche un mot comportant un accent, même si celui-ci fait parti de l'index, nous trouvons aucun résultat. C'est potentiellement dû à l'encodage des chaines de caractère dans Whoosh, nous avons essayer d'utiliser les méthodes `encode` et `decode` afin de palier le problème, mais rien n'y fait.

Voici deux mots clefs qui fonctionnent : "vote" et "proportionnel". Vous pouvez tester avec la commande `python3 src/main.py vote`.
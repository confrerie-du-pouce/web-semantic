#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

def populating_index():
  import csv
  import json
  import re
  from tqdm import tqdm
  from module.index import add_document

  print("## populating index ##")
  current = os.path.dirname(os.path.realpath(__file__))
  data = dict()
  with tqdm(os.listdir(os.path.join(current, "assets")), desc="Starting...", position=1) as entries:
    for entry in entries:
      entries.desc = entry
      with open(os.path.join(current, 'assets', entry), mode="r", encoding="utf8") as csvfile:
        reader = csv.DictReader(csvfile)
        for field in reader.fieldnames:
          if field.endswith('?') or field.endswith(':'):
            data[field] = dict()
        for row in reader:
          for question in data:
            answers = "Vote blanc"
            try:
              if row[question].strip():
                answers = row[question]
            except:
              pass
            for answer in answers.split('|'):
              if not answer in data[question].keys():
                data[question][answer] = 0
              data[question][answer] = data[question][answer] + 1
  for question in tqdm(data, desc="Saving questions", position=1):
    add_document(question.split(' - ')[1], data[question])

def main(*word: ("Keyword (s) to search", "positional")):
  """ Search entry in "Le Grand Débat"

  This script will initialize data if they doesn't exist.
  """
  from module.index import search, index_dir_exists, ix


  if not index_dir_exists():
    populating_index()

  research = ' '.join(word)
  print("Display first hit for \"{}\"".format(research))
  for hit in search(research):
    print(hit['question'])
    for answer in hit['answers']:
      input("Press enter to display next answer")
      print('  > {answer} : {number}'.format(answer=answer, number=hit['answers'][answer]))
    input("Press enter to display next question")

if __name__ == "__main__":
  import plac; plac.call(main)
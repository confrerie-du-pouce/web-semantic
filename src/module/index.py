# -*- coding: utf-8 -*-
from whoosh.index import create_in, open_dir, EmptyIndexError
from whoosh.fields import *
from whoosh.qparser import QueryParser
from tqdm import tqdm
import spacy
import os
import re
import numpy as np
import json

def init_index():
  return create_in(index_dir, schema)

def open_index():
  return open_dir(index_dir)

def index_dir_exists():
  return os.path.exists(index_dir)

def ix():
  if not index_dir_exists():
    os.mkdir(index_dir)
  try:
    return open_index()
  except EmptyIndexError:
    return init_index()

def purify(impure):
  """ Remove non-word character
  :param impure: an impure string that maybe contain some non-word character
  :return: a pure string without any non-word character all in lowercase
  """
  return re.sub("\W", " ", impure).lower()

def compare(main: str, search: str):
  """ Compare given string
  :param main: the string you want to compare with
  :param search: the string you search in main
  
  :rtype: int
  :return: pourcentage of similarity
  """
  return nlp(main).similarity(nlp(search))

def get_keywords(text: str):
  """ Extract keywords from given text
  :param text: text to extract keywords

  :rtype: list
  :return: list of keywords
  """
  result = []
  pos_tag = ["AUX", "ADP", "SYM", "NUM"]
  doc = nlp(purify(text))
  for token in doc:
    if token.pos_ in pos_tag:
      continue
    if not token.text in nlp.Defaults.stop_words and len(token.text) > 2:
      result.append(token.lemma_)
  return result

def add_document(question: str, answers: dict):
  """ Index a question with its answers
  :param question: a string that represent the question
  :param answers: a dict that contains question (str) as key and number (int) of vote as value
  """
  writer = ix().writer()
  keywords = []
  keywords = get_keywords(question)
  for answer in answers:
    keywords.get_keywords(answers)
  writer.add_document(keywords=keywords, question=question, answers=json.dumps(answers))
  writer.commit()

def most_similar(word: str):
  topn=len(word)
  ms = nlp.vocab.vectors.most_similar(
      nlp(word).vector.reshape(1,nlp(word).vector.shape[0]), n=topn)
  words = [nlp.vocab.strings[w] for w in ms[0][0]]
  distances = ms[2]
  return minify(words)

def minify(words: list):
  minified = set()
  for word in words:
    minified.add(purify(word))
  return list(minified)

def search(text: str):
  """ Search words within the index
  :param text: search sentence
  """
  keywords = get_keywords(text)
  stretch = []
  query = ""
  for keyword in keywords:
    stretch.append(most_similar(keyword))
  for words in stretch:
    if not query == "" and query.endswith(')'):
        query += " AND "
    for word in words:
      if not query.endswith(" AND ") and not query == "":
        query += " OR "
      else:
        query += "("
      query += " {w} ".format(w=word)
    query += ")"
  res = []
  query
  with ix().searcher() as searcher:
    parser = QueryParser("keywords", schema)
    query = parser.parse(query.encode())
    for hit in searcher.search(query):
      res.append({
        "question": hit["question"],
        "answers": json.loads(hit["answers"])
      })
  return res

schema = Schema(keywords=TEXT(stored=True), question=STORED, answers=STORED)
index_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')
nlp = None
try:
  nlp = spacy.load("fr_core_news_lg")
except OSError:
  try:
    nlp = spacy.load("fr_core_news_md")
  except OSError:
    try:
      nlp = spacy.load("fr_core_news_sm")
    except OSError:
      message = ["No model found.",
        "Please either download one of these model:",
        "\t> python3 -m spacy download fr_core_news_lg",
        "\tOR",
        "\t> python3 -m spacy download fr_core_news_md",
        "\tOR",
        "\t> python3 -m spacy download fr_core_news_sm",
        "The biggest one will be load next time."]
      raise OSError("\n".join(message))